package me.danielzgtg.compsci11_sem2_2017;

import java.util.Scanner;

/**
 * A program for stylizing the length of a movie.
 * 
 * @author Daniel Tang
 * @since 8 February 2017
 */
public class MovieLength {

	/**
	 * Prompt layout for movie length user input.
	 */
	private static final String USER_LENGTH_PROMPT =
			"How long (in whole seconds) is the movie? :";

	/**
	 * The error message when the user did not enter a valid integer length.
	 */
	private static final String USER_LENGTH_ERROR_MSG =
			"Sorry! I didn't understand that length, please try again.";

	/**
	 * The output layout for the sylized result.
	 */
	private static final String OUTPUT_LAYOUT =
			"%d hour%s, %d minute%s, %d second%s or %02d:%02d:%02d";

	public static void main(final String[] ignore) {
		final int lengthSeconds = promptForLength();

		// Seconds that do not make up a full minute
		final int outputSeconds = lengthSeconds % 60;
		// Total whole minutes (not including seconds)
		final int lengthMinutesFull = lengthSeconds / 60;
		// Minutes that do not make up a full hour
		final int outputMinutes = lengthMinutesFull % 60;
		// Whole hours (not including minutes or seconds)
		final int outputHours = lengthMinutesFull / 60;

		// Print out the result to console
		System.out.format(OUTPUT_LAYOUT,
				outputHours, outputHours == 1 ? "" : "s", // Length, (and plural support)
				outputMinutes, outputMinutes == 1 ? "" : "s",
				outputSeconds, outputSeconds == 1 ? "" : "s",
				outputHours, outputMinutes, outputSeconds); // HH:mm:ss
	}

	/**
	 * Obtains the movie length in whole seconds as a {@code int}
	 * 
	 * @return The movie length in whole seconds
	 */
	private static int promptForLength() {
		/*variable*/ String userInput;
		try (final Scanner scanner = new Scanner(System.in)) {
			while (true) {
				// Prompt the user and get the input
				System.out.print(USER_LENGTH_PROMPT);
				userInput = scanner.nextLine();

				try {
					// Try to return the result as an integer
					return Integer.valueOf(userInput);
				} catch (final NumberFormatException nfe) {
					// Keep asking until it is a valid integer
					System.out.println(USER_LENGTH_ERROR_MSG);
				}
			}
		}
	}
}
